import { Component, HostListener, Inject, OnInit, ViewChild } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';
import { WINDOW } from '../services/window/window.service';
import { ElementRef } from '@angular/core';
import { FormularioComponent } from '../formulario/formulario.component';


@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html'
})
export class MenuComponent implements OnInit {
  @ViewChild('myMenu') el: ElementRef;
  @ViewChild(FormularioComponent) formularioComponent: FormularioComponent;
  public navIsFixed = false;

  constructor( @Inject(DOCUMENT) private document: Document,
    @Inject(WINDOW) private window) { }

  ngOnInit() { }

  @HostListener('window:scroll', [])
  onWindowScroll(): void {
    const number = this.window.pageYOffset;
    if (number === this.el.nativeElement.offsetTop) {
      this.formularioComponent.openForm = false;
    } else if (this.window.pageYOffset === 0) {
      this.formularioComponent.openForm = true;
    }
  }
}
