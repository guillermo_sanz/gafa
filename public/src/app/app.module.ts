import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { WINDOW_PROVIDERS } from './services/window/window.service';
import { UserService } from './services/user/user.service';
import { PropertiesService } from './services/properties/properties.service';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { SliderComponent } from './slider/slider.component';
import { MapComponent } from './map/map.component';
import { MenuComponent } from './menu/menu.component';
import { FormularioComponent } from './formulario/formulario.component';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { AlertService } from './services/alert/alert.service';
import { AlertComponent } from './directives/alert/alert.component';
import { PropertiesComponent } from './properties/properties.component';
import { AgmCoreModule } from '@agm/core';



@NgModule({
  declarations: [
    AppComponent,
    SliderComponent,
    MapComponent,
    MenuComponent,
    FormularioComponent,
    AlertComponent,
    PropertiesComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDQDuF_SHeIpZGzB77F9EcGzvzymyk3-FA'
    })
  ],
  providers: [WINDOW_PROVIDERS, UserService, AlertService, PropertiesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
