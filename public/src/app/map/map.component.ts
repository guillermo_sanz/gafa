import { Component, OnInit } from '@angular/core';
import { AgmCoreModule } from '@agm/core';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html'
})
export class MapComponent implements OnInit {
  lat: number;
  lng: number;
  zoom: number;

  constructor() {
    this.lat = 19.4191377;
    this.lng = -99.164345;
    this.zoom = 10;
  }

  ngOnInit() { }

}
