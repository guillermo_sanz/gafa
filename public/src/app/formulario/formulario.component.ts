import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../services/user/user.service';
import { User } from '../models/user';
import { AlertService } from '../services/alert/alert.service';
import { Subscription } from 'rxjs/Subscription';


@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html'
})
export class FormularioComponent implements OnInit {
  openForm: boolean;
  userForm: FormGroup;
  userObs: Subscription;

  constructor(private fb: FormBuilder, private userService: UserService, private alertService: AlertService) {
    this.openForm = true;
    this.userForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  ngOnInit() {
  }
  onSubmit(): void {
    const user = new User();
    user.fromForm(this.userForm);
    this.userObs = this.userService.login(user).subscribe(data => {
      if (data.user) {
        this.alertService.success('Usuario registrado.', true);
      } else {
        this.alertService.error('Usuario no registrado.', true);
      }
    });
    this.userForm.reset();
  }
  ngOnDestroy() {
    this.userObs.unsubscribe();
  }

}
