import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class PropertiesService {

  constructor(private http: Http) { }
  getProperties(): Observable<any> {
    return this.http.get('http://jsonplaceholder.typicode.com/posts').map((response: Response) => response.json());
  }
}
