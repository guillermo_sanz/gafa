import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { User } from '../../models/user';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class UserService {

  constructor(private http: Http) { }

  login(user: User): Observable<any> {
    return this.http.post('/api/user/authenticate-user', user).map((response: Response) => response.json());
  }

}
