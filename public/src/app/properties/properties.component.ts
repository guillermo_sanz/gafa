import { Component, OnInit } from '@angular/core';
import { PropertiesService } from '../services/properties/properties.service';

@Component({
  selector: 'app-properties',
  templateUrl: './properties.component.html'
})
export class PropertiesComponent implements OnInit {
  constructor(private propertiesService: PropertiesService) { }
  properties: string[];
  ngOnInit() {
    this.propertiesService.getProperties().subscribe(data => {
      this.properties = data;
    });
  }
}
