import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html'
})
export class SliderComponent implements OnInit {
  imagesSlider: any;

  constructor() {
    this.imagesSlider = [
      {
        src: '../assets/cabañas.jpg',
        alt: 'Cabañas',
        active: true
      },
      {
        src: '../assets/sea.jpg',
        alt: 'Sea',
        active: false
      },
      {
        src: '../assets/boat.jpg',
        alt: 'Boat',
        active: false
      }
    ];
  }

  ngOnInit() {
  }

}
