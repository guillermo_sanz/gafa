var express = require('express');
var router = express.Router();
var ctrlUsers = require('../controllers/user.controller.js');
router.post('/authenticate-user', ctrlUsers.user);

module.exports = router;
